const root = document.getElementById('root');
const header = document.createElement('header');
const buttonMain = document.createElement('button');
const buttonSingIn = document.createElement('button');
const buttonRegister = document.createElement('button');
const wrapperButtonHeader = document.createElement('div');

const wrapperReg = document.createElement('div');
const inputLogin = document.createElement('input');
const inputPass = document.createElement('input');
const inputPassConf = document.createElement('input');
const wrappperButtonReg = document.createElement('div');
const buttonSave = document.createElement('button');
const buttonReset = document.createElement('button');
const wrapperLogin = document.createElement('div');

const messageSingIn = document.createElement('div');
const messageError = document.createElement('div');
const message = document.createElement('p');

const buttonEnter = document.createElement('button');
const buttonSingOut = document.createElement('button');

const buttonPlay = document.createElement('button');
const buttonExite = document.createElement('button');

const wrapperPlayer = document.createElement('div');
const imgPlayer = document.createElement('img');
const player = document.createElement('span');

const wrapperScore = document.createElement('span');
let point = 40;

const condition = document.createElement('span');
condition.classList.add('condition');

const wrapperGame = document.createElement('div');
const imgTask = document.createElement('img');
const wrapperAnswers = document.createElement('div');
const answer4 = document.createElement('input');
const buttonOk = document.createElement('button'); // кнопка ок//

const wrapperCondition = document.createElement('div');

let loginUnique = true;  //флаг иникальности логина//
let loginPassInUsers = false;  //флаг наличия логина и пароля в LocalStorage//
let isLoginlS  = false;  //флаг св-ва isLogin в localStorage//
let loginLS = false;   //флаг св-ва login в localStorage//

messageSingIn.classList.add('messageSingIn');
messageError.classList.add('messageError');
message.classList.add('message');
wrapperButtonHeader.classList.add('wrapperButtonHeader');

root.classList.add('root-element', 'bgMain', 'dislpayFlex');

wrapperCondition.classList.add('wrapperCondition');

const removeMessag = () => {
  messageSingIn.remove();
  messageError.remove();
  inputLogin.classList.remove('invalid', 'icon');
  inputPass.classList.remove('invalid', 'icon');
  inputPassConf.classList.remove('invalid', 'icon');
}

(drawMenu = () => {
  header.classList.add('header');
  buttonMain.classList.add('button');
  buttonSingIn.classList.add('button');
  buttonRegister.classList.add('button');
  root.classList.add('bgMain');

  buttonMain.innerHTML = 'Main';
  buttonSingIn.innerHTML = 'Sing in';
  buttonRegister.innerHTML = 'Register';

  wrapperButtonHeader.append(buttonMain, buttonRegister, buttonSingIn);
  header.append(wrapperButtonHeader);
  root.append(header);
  
})();

const clickReset = () => {
  inputLogin.value = '';
  inputPass.value = '';
  inputPassConf.value = '';

  removeMessag();
}

//Проверяю в localStorage, есть ли кто то залогиненный//
const checkIsLogin =() => {
  let users  = localStorage.users ? JSON.parse(localStorage.users) : [];

  users .forEach((element) => {
    if (element.isLogin === true) {
      isLoginlS  = true;
    }
    return isLoginlS ;
  })
}

//Проверяю в localStorage, есть ли такой логин//
const checkLogin =() => {
  let users  = localStorage.users ? JSON.parse(localStorage.users) : [];

  users.forEach((element) => {
    if (element.login === inputLogin.value) {
      loginLS  = true;
    }
    return loginLS ;
  })
}

const clickMain = () => {
  root.classList.remove('bgRegister');
  root.classList.add('bgMain');

  while (root.firstChild) {
    root.firstChild.remove();
  }

  checkIsLogin();

  if(isLoginlS ) {
    header.classList.add('header');
    buttonMain.classList.add('button');
    buttonPlay.classList.add('button');
    buttonSingOut.classList.add('button');
    root.classList.add('bgMain');

    buttonSingOut.innerHTML = 'Sing out';
    buttonPlay.innerHTML = 'Play';
    buttonMain.innerHTML = 'Main';
  
    drawPlayer();
    drawScore();
    wrapperButtonHeader.append(buttonMain, buttonPlay, buttonSingOut);
    header.append(wrapperButtonHeader, wrapperScore, wrapperPlayer);
    root.append(header);
  } else {
    drawMenu();
    buttonSingOut.remove();
    buttonPlay.remove();
    buttonExite.remove();
    wrapperPlayer.remove();
    wrapperScore.remove();
  }
}

const clickRegister = () => {

  wrapperLogin.remove();

  wrapperReg.classList.add('wrapperReg');
  inputLogin.classList.add('input');
  inputPass.classList.add('input');
  inputPassConf.classList.add('input');
  wrappperButtonReg.classList.add('wrappperButtonReg');
  buttonSave.classList.add('button');
  buttonReset.classList.add('button');
  root.classList.add('bgRegister');

  inputLogin.setAttribute('type', 'text');
  inputLogin.setAttribute('placeholder', 'Enter Login');
  inputPass.setAttribute('type', 'password');
  inputPass.setAttribute('placeholder', 'Enter Password');
  inputPassConf.setAttribute('type', 'password');
  inputPassConf.setAttribute('placeholder', 'Confirm Password');

  buttonSave.innerHTML = 'Save';
  buttonReset.innerHTML = 'Reset';

  wrapperReg.append(inputLogin, inputPass, inputPassConf, wrappperButtonReg);
  wrappperButtonReg.append(buttonSave, buttonReset);
  root.append(wrapperReg);

  clickReset();
}

//Проверяю на совпадение inputPass и inputPassConf//
const checkPassword = () => {
  for (var i = 0; i <= inputLogin.value.length; i++) {
    if (inputPass.value.indexOf(inputPassConf.value) != 0) {
      message.innerHTML = 'Пароли не совпадают, попробуйте ввести пароль еще раз';
      messageError.append(message);
      wrapperReg.prepend(messageError);
      inputPass.classList.add('invalid', 'icon');
      inputPassConf.classList.add('invalid', 'icon');
      inputPassConf.value = '';
    }
  }
}

const clickSave = () => {
  let users = localStorage.users ? JSON.parse(localStorage.users) : [];

  const user = {
    login: inputLogin.value, 
    password: inputPass.value,
    isLogin: false,
    score: 40
  }

  checkLogin();

  if (loginLS === true) {
    message.innerHTML = 'Такой логин уже существует';
    messageError.append(message);
    wrapperReg.prepend(messageError);
    inputLogin.classList.add('invalid', 'icon');
    loginLS = false;
  } else if (inputLogin.value === '') {
    message.innerHTML = 'Заполните логин';
    messageError.append(message);
    wrapperReg.prepend(messageError);
    inputLogin.classList.add('invalid', 'icon');
  } else if (inputPassConf.value === '') {
    message.innerHTML = 'Повторите пароль';
    messageError.append(message);
    wrapperReg.prepend(messageError);
    inputPassConf.classList.add('invalid', 'icon');
  } else {
    message.innerHTML = 'Логин и пароль успешно сохранены';
    wrapperReg.remove();
    messageSingIn.append(message);
    root.append(messageSingIn);

    users.push(user);
    localStorage.setItem('users', JSON.stringify(users));
    users = JSON.parse(localStorage.getItem('users'));
  }
}

const clickSingIn = () => {
  wrapperLogin.classList.add('wrapperLogin');
  inputLogin.classList.add('input');
  inputPass.classList.add('input');
  buttonEnter.classList.add('button');
  root.classList.add('bgRegister');

  buttonEnter.innerHTML = 'Enter';

  inputLogin.setAttribute('type', 'text');
  inputLogin.setAttribute('placeholder', 'Enter Login');
  inputPass.setAttribute('placeholder', 'Enter Password');
  inputPass.setAttribute('type', 'password');

  while (root.children[1]) {
    root.children[1].remove();
  }
  clickReset();

  wrapperLogin.append(inputLogin, inputPass, buttonEnter);
  root.append(wrapperLogin);
}

//Проверка, что человек был ранее зарегистрирован//
const isLoginPassInUsers = () => {
  let users = localStorage.users ? JSON.parse(localStorage.users) : [];

  users.forEach((element) => {
    if ((inputLogin.value === element.login) && (inputPass.value === element.password)) {
      loginPassInUsers = true;
    }
    return loginPassInUsers;
  })
}

//Меняет значение свойства isLogin на 'true' при входе под логином//
const isSingIn = () => {
  let users = localStorage.users ? JSON.parse(localStorage.users) : [];
  let singIn = false;

  users.forEach((element) => {
    if ((inputLogin.value === element.login) && (inputPass.value === element.password)) {
      singIn = true;
    }
    if(singIn) {
      element.isLogin = true;
      localStorage.setItem('users', JSON.stringify(users));
      users = JSON.parse(localStorage.getItem('users'));
    }
  })
}

//Вид окна, когда выполнен вход под логином//
const windowSingIn = () => {
  while (wrapperButtonHeader.children[0]) {
    wrapperButtonHeader.children[0].remove();
  }
  
  clickMain();
  message.innerHTML = `Приветствуем вас, ${inputLogin.value}, в нашей игре`;

  messageSingIn.append(message);
  root.append (messageSingIn);
  wrapperLogin.remove();

  while (wrapperLogin.children[1]) {
    wrapperLogin.children[1].remove();
  }
}

const clickEnter = () => {
  isLoginPassInUsers();

  if (!loginPassInUsers) {
    message.innerHTML = 'Неверный логин или пароль';
    messageError.append(message);
    wrapperLogin.prepend(messageError);
    inputLogin.classList.add('invalid', 'icon');
    inputPass.classList.add('invalid', 'icon');
    inputPass.value = '';
  } else if (inputLogin.value === '') {
    message.innerHTML = 'Введите логин';
    messageError.append(message);
    wrapperLogin.prepend(messageError);
    inputLogin.classList.add('invalid', 'icon');
  } else if (inputPass.value === '') {
    message.innerHTML = 'Введите пароль';
    messageError.append(message);
    wrapperLogin.prepend(messageError);
    inputPass.classList.add('invalid', 'icon');
  } else {
    isSingIn();
    windowSingIn();
  }
}

//Рисует аватар//
const drawPlayer = () => {
  let users  = localStorage.users ? JSON.parse(localStorage.users) : [];
  users.forEach((element) => {
    if(element.isLogin == true) {
      wrapperPlayer.classList.add('wrapperPlayer');
      imgPlayer.classList.add('imgPlayer');
      player.classList.add('player');

      imgPlayer.src = './avatar.png'
      player.innerText = element.login;

      wrapperPlayer.append(imgPlayer, player);
      localStorage.setItem('users', JSON.stringify(users));
      users = JSON.parse(localStorage.getItem('users'));
    }
  })
}

//Добавила поле со счетом//
const drawScore = () => {
  let users  = localStorage.users ? JSON.parse(localStorage.users) : [];
  users.forEach((element) => {
    if(element.isLogin == true) {
      wrapperScore.classList.add('wrapperScore');
      wrapperScore.innerHTML = `SCORE: ${element.score}`;
      localStorage.setItem('users', JSON.stringify(users));
      users = JSON.parse(localStorage.getItem('users'));
    }
  })
}

//Задание//
const messageCondition = () => {
  const wrapperCondition = document.createElement('div');
  wrapperCondition.classList.add('wrapperCondition');
  condition.innerText = 'Найдите потерянный фрагмент пазла';

  setTimeout(() => {
    wrapperCondition.append(condition);
    root.append(wrapperCondition);

    setTimeout(() => {
      wrapperCondition.remove();
    },1500);
  },500);
}

const addTask = () => {

  //Первый фрагмент//
  const answer1 = document.createElement('input');
  const labelAnswer1 = document.createElement('label');
  const img1 = document.createElement('img');

  answer1.classList.add('answer');
  img1.classList.add('img');
  answer1.setAttribute('type', 'radio');
  answer1.setAttribute('name', 'prim');
  img1.src = './part1.jpg';

  labelAnswer1.append(answer1, img1);

  //Второй фрагмент//
  const answer2 = document.createElement('input');
  const labelAnswer2 = document.createElement('label');
  const img2 = document.createElement('img');

  answer2.classList.add('answer');
  img2.classList.add('img');
  answer2.setAttribute('type', 'radio');
  answer2.setAttribute('name', 'prim');
  img2.src = './part2.jpg';

  labelAnswer2.append(answer2, img2);

  //Третий фрагмент//
  const answer3 = document.createElement('input');
  const labelAnswer3 = document.createElement('label');
  const img3 = document.createElement('img');

  answer3.classList.add('answer');
  img3.classList.add('img');
  answer3.setAttribute('type', 'radio');
  answer3.setAttribute('name', 'prim');
  img3.src = './part3.jpg';

  labelAnswer3.append(answer3, img3);

  //Четвертый фрагмент//
  const labelAnswer4 = document.createElement('label');
  const img4 = document.createElement('img');

  answer4.classList.add('answer');
  img4.classList.add('img');
  answer4.setAttribute('type', 'radio');
  answer4.setAttribute('name', 'prim');
  img4.src = './part4.jpg';

  labelAnswer4.append(answer4, img4);
  wrapperAnswers.append(labelAnswer1, labelAnswer2, labelAnswer3, labelAnswer4);
}

const task = () => {
  wrapperGame.classList.add('wrapperGame');
  imgTask.classList.add('imgTask');
  wrapperAnswers.classList.add('wrapperAnswers');
  buttonOk.classList.add('button');

  buttonOk.innerHTML = 'OK';
  imgTask.src = './taskLine.jpg';

  addTask();
  wrapperGame.append(imgTask, wrapperAnswers, buttonOk);
  root.append(wrapperGame);
}

//Не правльный фрагмент//
const messageMistake = () => {
  condition.innerText = 'Вы ошиблись и потеряли 20 баллов, попробуйте еще раз';

  setTimeout(() => {
    wrapperCondition.append(condition);
    root.append(wrapperCondition);

    setTimeout(() => {
      wrapperCondition.remove();
    },1500);
  },500);
}

const drawMistake = () => {
  while (wrapperAnswers.children[0]) {
    wrapperAnswers.children[0].remove();
  }

  while (root.children[1]) {
    root.children[1].remove();
  }

  header.classList.add('headerPlay');
  messageMistake();
  setTimeout(() => {
    task();
  },2000);

}

const checkScore = () => {
  let users = localStorage.users ? JSON.parse(localStorage.users) : [];
  users.forEach((element) => {
    if(element.score == 0) {
      while (wrapperAnswers.children[0]) {
        wrapperAnswers.children[0].remove();
      }
    
      while (root.children[1]) {
        root.children[1].remove();
      }
    
      header.classList.add('headerPlay');
      clickExite();
    }
  })
}

const checkAnswer = () => {
  let users = localStorage.users ? JSON.parse(localStorage.users) : [];
  users.forEach((element) => {
    if (answer4.checked) {
      clickOk();

      setTimeout(() => {
        root.classList.add('bgWon');
        root.classList.remove('bgPlay');
        wrapperScore.remove();
      },3000);
    } else {
      if(element.isLogin == true) {

        if (element.score >= 20) {
          element.score = point - 20;
          point = point - 20;
          localStorage.setItem('users', JSON.stringify(users));
          users = JSON.parse(localStorage.getItem('users'));  
          drawScore();
          drawMistake();    
        }
        if (element.score == 0) {
          while (wrapperAnswers.children[0]) {
            wrapperAnswers.children[0].remove();
          }
        
          while (root.children[1]) {
            root.children[1].remove();
          }
          header.classList.add('headerPlay');
          const imgLost = document.createElement('img');
          imgLost.src = './lost.jpg';
          root.append(imgLost);
        }
      }
    }
  })
}

const clickPlay = () => {

  while (wrapperButtonHeader.children[0]) {
    wrapperButtonHeader.children[0].remove();
  }

  while (wrapperAnswers.children[0]) {
    wrapperAnswers.children[0].remove();
  }

  buttonExite.classList.add('button');
  buttonExite.innerHTML = 'Exite';
  wrapperButtonHeader.append(buttonExite);
  
  while (root.children[1]) {
    root.children[1].remove();
  }

  header.classList.add('headerPlay');
  root.classList.add('bgPlay');
  root.classList.remove('bgMain');
  messageWon.remove();

  let users = localStorage.users ? JSON.parse(localStorage.users) : [];
  users.forEach((element) => {
    if(element.isLogin == true) {
      element.score = 40;
      point = 40;
      localStorage.setItem('users', JSON.stringify(users));
      users = JSON.parse(localStorage.getItem('users'));
    }
    drawScore();
  })
  messageCondition();
  setTimeout(() => {
    task();
  },2000);
}

const messageWon = document.createElement('span');

const clickOk = () => {

  setTimeout(() => {
    imgTask.src = './task.jpg';

    setTimeout(() => {
      imgTask.remove();
      messageWon.classList.add('messageWon');
      messageWon.innerText = `Вы победили со счетом ${point} баллов! Поздравляем!`;
      wrapperGame.append(messageWon);
    },2500);
  },500);

  buttonOk.remove();
  wrapperAnswers.remove();
  wrapperGame.classList.add('center');
}

const clickExite = () => {
  clickMain();
  buttonExite.remove();
  wrapperGame.remove();
  root.classList.remove('bgWon');
  root.classList.remove('bgPlay');
  root.classList.add('bgMain');
}

//Меняет значение свойства isLogin на 'false' при выходе//
const isSingOut = () => {
  let users = localStorage.users ? JSON.parse(localStorage.users) : [];
  
  users.forEach((element) => {
    if(element.isLogin==true ) {
      isLoginlS  = false;
      element.isLogin = false;
      loginPassInUsers = false;
      localStorage.setItem('users', JSON.stringify(users));
      users = JSON.parse(localStorage.getItem('users'));
    }
  })
}

const clickSingOut = () => {
  isSingOut();
  clickMain();
}

const checkLoad = () => {
  checkIsLogin();
  while (wrapperButtonHeader.children[0]) {
    wrapperButtonHeader.children[0].remove();
  }
  clickMain();
}

window.addEventListener('load', checkLoad);
inputLogin.addEventListener('click', removeMessag);
inputPass.addEventListener('click', removeMessag);
inputPassConf.addEventListener('click', removeMessag);
buttonSingIn.addEventListener('click', clickSingIn);
buttonMain.addEventListener('click', clickMain);
buttonEnter.addEventListener('click', clickEnter);
buttonRegister.addEventListener('click', clickRegister, removeMessag);
inputPassConf.addEventListener('keyup', checkPassword);
buttonReset.addEventListener('click', clickReset);
buttonSave.addEventListener('click', clickSave);
buttonPlay.addEventListener('click', clickPlay);
buttonExite.addEventListener('click', clickExite);
buttonSingOut.addEventListener('click', clickSingOut);
buttonOk.addEventListener('click', checkAnswer);